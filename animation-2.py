import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from mysql.connector import MySQLConnection, Error
from python_mysql_dbconfig import read_db_config
import time


def memory_global_total():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute("select * from sys.memory_global_total")
        rows = cursor.fetchall()
        for row in rows:
            r = row[0]
            mem_gl = int(float(str(r.split()[0])))
            return mem_gl

    except Error as e:
        return -1
    conn.close()


def buffer_pages():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute("select pages from sys.innodb_buffer_stats_by_schema")
        rows = cursor.fetchall()
        buf_pag = 0
        for row in rows:
            buf_pag += int(row[0])
        
        return buf_pag

    except Error as e:
        return -1
    conn.close()



def lock_wait():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute("select * from sys.innodb_lock_waits")
        rows = cursor.fetchall()
        return len(rows)

    except Error as e:
        return -1
    conn.close()



def temp_table():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "created_tmp_tables"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()



def dirty_pages():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "innodb_buffer_pool_pages_dirty"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()


def pages_data():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        
        cursor = conn.cursor()
        cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "innodb_buffer_pool_pages_data"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()



def write_req_buff():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "innodb_buffer_pool_write_requests"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()



def pages_written():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "innodb_pages_written"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()


def queries():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "queries"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()



def full_join():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "select_full_join"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()


def sort_merge_passes():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "sort_merge_passes"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()



def table_wait_lock():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "table_locks_waited"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()



def threads_running():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "threads_running"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()



def locks_deadlocks():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "locks_deadlocks"')
        rows = cursor.fetchall()
        if (len(rows)>0):
            ins =int(rows[0][0])
        else:
            ins =0
        return ins

    except Error as e:
        return -1
    conn.close()



def full_table_scan():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute("select * from sys.statements_with_full_table_scans")
        rows = cursor.fetchall()
        return len(rows)

    except Error as e:
        return -1
    conn.close()


def sorting():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute("select * from sys.statements_with_sorting")
        rows = cursor.fetchall()
        return len(rows)

    except Error as e:
        return -1
    conn.close()



def row_update():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select Variable_value from sys.metrics m where m.Variable_name = "innodb_rows_updated"')
        rows = cursor.fetchall()
        buf_pag = 0
        for row in rows:
            ins = row[0]
        ins =int(ins)
        return ins

    except Error as e:
        return -1
    conn.close()



def query_with_fetchall():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute("SHOW GLOBAL STATUS;")
        # cursor.execute("select * from information_schema.statistics")
        # cursor.execute("select * from sys.memory_global_total")
        rows = cursor.fetchall()
        print('Total Row(s):', cursor.rowcount)
        for row in rows:
            print(row)

    except Error as e:
        print(e)


###################################################################1
fig1 = plt.figure()

ax_memory_global = fig1.add_subplot(141)
xdata_memory_global, ydata_memory_global = [], []
ln_memory_global, = ax_memory_global.plot([], [])

ax_buffer_pages = fig1.add_subplot(142)
xdata_buffer_pages, ydata_buffer_pages = [], []
ln_buffer_pages, = ax_buffer_pages.plot([], [], 'g-')

ax_row_update = fig1.add_subplot(143)
xdata_row_update, ydata_row_update = [], []
ln_row_update, = ax_row_update.plot([], [], 'r-')

ax_lock_wait = fig1.add_subplot(144)
xdata_lock_wait, ydata_lock_wait = [], []
ln_lock_wait, = ax_lock_wait.plot([], [], 'r-')

def init1():
    ln_memory_global.set_data([], [])
    ln_buffer_pages.set_data([], [])
    ln_row_update.set_data([], [])
    ln_row_update.set_data([], [])
    
    ax_memory_global.set_xlim(0, 50)
    ax_memory_global.set_ylim(-2, 100)
    ax_memory_global.set_ylabel('memory global')

    ax_buffer_pages.set_xlim(0, 50)
    ax_buffer_pages.set_ylim(-2, 100)
    ax_buffer_pages.set_ylabel('buffer pages')
    
    ax_row_update.set_xlim(0, 50)
    ax_row_update.set_ylim(-2, 100)
    ax_row_update.set_ylabel('row update')

    ax_lock_wait.set_xlim(0, 50)
    ax_lock_wait.set_ylim(-2, 100)
    ax_lock_wait.set_ylabel('lock wait')

    return ln_memory_global, ln_buffer_pages, ln_row_update, ln_lock_wait

def update1(frame):
    xdata_memory_global.append(time.clock())
    ydata_memory_global.append(memory_global_total())
    ln_memory_global.set_data(xdata_memory_global, ydata_memory_global)

    xdata_buffer_pages.append(time.clock())
    ydata_buffer_pages.append(buffer_pages())
    ln_buffer_pages.set_data(xdata_buffer_pages, ydata_buffer_pages)

    xdata_row_update.append(time.clock())
    ydata_row_update.append(row_update())
    ln_row_update.set_data(xdata_row_update, ydata_row_update)

    xdata_lock_wait.append(time.clock())
    ydata_lock_wait.append(lock_wait())
    ln_lock_wait.set_data(xdata_lock_wait, ydata_lock_wait)

    return  ln_memory_global, ln_buffer_pages, ln_row_update, ln_lock_wait,

ani1 = FuncAnimation(fig1, update1, frames=1,
                    init_func=init1, blit=True, interval =80)
plt.show()

###########################################################

fig2 = plt.figure()

ax_full_table_scan = fig2.add_subplot(141)
xdata_full_table_scan, ydata_full_table_scan = [], []
ln_full_table_scan, = ax_full_table_scan.plot([], [], 'r-')

ax_sorting = fig2.add_subplot(142)
xdata_sorting, ydata_sorting = [], []
ln_sorting, = ax_sorting.plot([], [], 'g-')

ax_temp_table = fig2.add_subplot(143)
xdata_temp_table, ydata_temp_table = [], []
ln_temp_table, = ax_temp_table.plot([], [], 'r-')

ax_dirty_pages = fig2.add_subplot(144)
xdata_dirty_pages, ydata_dirty_pages = [], []
ln_dirty_pages, = ax_dirty_pages.plot([], [], 'r-')

def init2():
    ln_full_table_scan.set_data([], [])
    ln_sorting.set_data([], [])
    ln_temp_table.set_data([], [])
    ln_dirty_pages.set_data([], [])
    
    ax_full_table_scan.set_xlim(0, 50)
    ax_full_table_scan.set_ylim(-2, 100)
    ax_full_table_scan.set_ylabel('full table scan')

    ax_sorting.set_xlim(0, 50)
    ax_sorting.set_ylim(-2, 100)
    ax_sorting.set_ylabel('sorting')
    
    ax_temp_table.set_xlim(0, 50)
    ax_temp_table.set_ylim(-2, 100)
    ax_temp_table.set_ylabel('temp table')

    ax_dirty_pages.set_xlim(0, 50)
    ax_dirty_pages.set_ylim(-2, 100)
    ax_dirty_pages.set_ylabel('dirty pages')

    return ln_full_table_scan, ln_sorting, ln_temp_table, ln_dirty_pages

def update2(frame):
    xdata_full_table_scan.append(time.clock())
    ydata_full_table_scan.append(full_table_scan())
    ln_full_table_scan.set_data(xdata_full_table_scan, ydata_full_table_scan)

    xdata_sorting.append(time.clock())
    ydata_sorting.append(sorting())
    ln_sorting.set_data(xdata_sorting, ydata_sorting)

    xdata_temp_table.append(time.clock())
    ydata_temp_table.append(temp_table())
    ln_temp_table.set_data(xdata_temp_table, ydata_temp_table)

    xdata_dirty_pages.append(time.clock())
    ydata_dirty_pages.append(dirty_pages())
    ln_dirty_pages.set_data(xdata_dirty_pages, ydata_dirty_pages)

    return  ln_full_table_scan, ln_sorting, ln_temp_table, ln_dirty_pages,

ani2 = FuncAnimation(fig2, update2, frames=1,
                    init_func=init2, blit=True, interval =80)

plt.show()
##################################################################

fig3 = plt.figure()

ax_pages_data = fig3.add_subplot(141)
xdata_pages_data, ydata_pages_data = [], []
ln_pages_data, = ax_pages_data.plot([], [], 'r-')

ax_write_req_buff = fig3.add_subplot(142)
xdata_write_req_buff, ydata_write_req_buff = [], []
ln_write_req_buff, = ax_write_req_buff.plot([], [], 'g-')

ax_pages_written = fig3.add_subplot(143)
xdata_pages_written, ydata_pages_written = [], []
ln_pages_written, = ax_pages_written.plot([], [], 'r-')

ax_queries = fig3.add_subplot(144)
xdata_queries, ydata_queries = [], []
ln_queries, = ax_queries.plot([], [], 'r-')

def init3():
    ln_pages_data.set_data([], [])
    ln_write_req_buff.set_data([], [])
    ln_pages_written.set_data([], [])
    ln_queries.set_data([], [])
    
    ax_pages_data.set_xlim(0, 50)
    ax_pages_data.set_ylim(-2, 100)
    ax_pages_data.set_ylabel('pages data')

    ax_write_req_buff.set_xlim(0, 50)
    ax_write_req_buff.set_ylim(-2, 100)
    ax_write_req_buff.set_ylabel('write req buff')
    
    ax_pages_written.set_xlim(0, 50)
    ax_pages_written.set_ylim(-2, 100)
    ax_pages_written.set_ylabel('pages written')

    ax_queries.set_xlim(0, 50)
    ax_queries.set_ylim(-2, 100)
    ax_queries.set_ylabel('queries')

    return ln_pages_data, ln_write_req_buff, ln_pages_written, ln_queries,

def update3(frame):
    xdata_pages_data.append(time.clock())
    ydata_pages_data.append(pages_data())
    ln_pages_data.set_data(xdata_pages_data, ydata_pages_data)

    xdata_write_req_buff.append(time.clock())
    ydata_write_req_buff.append(write_req_buff())
    ln_write_req_buff.set_data(xdata_write_req_buff, ydata_write_req_buff)

    xdata_pages_written.append(time.clock())
    ydata_pages_written.append(pages_written())
    ln_pages_written.set_data(xdata_pages_written, ydata_pages_written)

    xdata_queries.append(time.clock())
    ydata_queries.append(queries())
    ln_queries.set_data(xdata_queries, ydata_queries)

    return  ln_pages_data, ln_write_req_buff, ln_pages_written, ln_queries,

ani3 = FuncAnimation(fig3, update3, frames=1,
                    init_func=init3, blit=True, interval =80)
plt.show()
##########################################################13

fig4 = plt.figure()

ax_full_join = fig4.add_subplot(141)
xdata_full_join, ydata_full_join = [], []
ln_full_join, = ax_full_join.plot([], [], 'r-')

ax_sort_merge_passes = fig4.add_subplot(142)
xdata_sort_merge_passes, ydata_sort_merge_passes = [], []
ln_sort_merge_passes, = ax_sort_merge_passes.plot([], [], 'g-')

ax_table_wait_lock = fig4.add_subplot(143)
xdata_table_wait_lock, ydata_table_wait_lock = [], []
ln_table_wait_lock, = ax_table_wait_lock.plot([], [], 'r-')

ax_threads_running = fig4.add_subplot(144)
xdata_threads_running, ydata_threads_running = [], []
ln_threads_running, = ax_threads_running.plot([], [], 'r-')

def init4():
    ln_full_join.set_data([], [])
    ln_sort_merge_passes.set_data([], [])
    ln_table_wait_lock.set_data([], [])
    ln_threads_running.set_data([], [])
    
    ax_full_join.set_xlim(0, 50)
    ax_full_join.set_ylim(-2, 100)
    ax_full_join.set_ylabel('full join')

    ax_sort_merge_passes.set_xlim(0, 50)
    ax_sort_merge_passes.set_ylim(-2, 100)
    ax_sort_merge_passes.set_ylabel('sort merge passes')
    
    ax_table_wait_lock.set_xlim(0, 50)
    ax_table_wait_lock.set_ylim(-2, 100)
    ax_table_wait_lock.set_ylabel('table wait lock')

    ax_threads_running.set_xlim(0, 50)
    ax_threads_running.set_ylim(-2, 100)
    ax_threads_running.set_ylabel('threads running')

    return ln_full_join, ln_sort_merge_passes, ln_table_wait_lock, ln_threads_running,

def update4(frame):
    xdata_full_join.append(time.clock())
    ydata_full_join.append(full_join())
    ln_full_join.set_data(xdata_full_join, ydata_full_join)

    xdata_sort_merge_passes.append(time.clock())
    ydata_sort_merge_passes.append(sort_merge_passes())
    ln_sort_merge_passes.set_data(xdata_sort_merge_passes, ydata_sort_merge_passes)

    xdata_table_wait_lock.append(time.clock())
    ydata_table_wait_lock.append(table_wait_lock())
    ln_table_wait_lock.set_data(xdata_table_wait_lock, ydata_table_wait_lock)

    xdata_threads_running.append(time.clock())
    ydata_threads_running.append(threads_running())
    ln_threads_running.set_data(xdata_threads_running, ydata_threads_running)

    return  ln_full_join, ln_sort_merge_passes, ln_table_wait_lock, ln_threads_running,

ani4 = FuncAnimation(fig4, update4, frames=1,
                    init_func=init4, blit=True, interval =80)
plt.show()
##########################################################17
fig_locks_deadlocks = plt.figure()
ax_locks_deadlocks = plt.axes(xlim=(0, 50), ylim=(-2, 100))
xdata_locks_deadlocks, ydata_locks_deadlocks = [], []
ln_locks_deadlocks, = ax_locks_deadlocks.plot([], [], lw=2)

def init_locks_deadlocks():
    ln_locks_deadlocks.set_data([], [])
    return ln_locks_deadlocks,

def update_locks_deadlocks(frame):
    xdata_locks_deadlocks.append(time.clock())
    #p (time.clock())
    ydata_locks_deadlocks.append(locks_deadlocks())
    ln_locks_deadlocks.set_data(xdata_locks_deadlocks, ydata_locks_deadlocks)
    return ln_locks_deadlocks,

ani_locks_deadlocks = FuncAnimation(fig_locks_deadlocks, update_locks_deadlocks, frames=1,
                    init_func=init_locks_deadlocks, blit=True, interval =80)
plt.show()
################################################################################OS1

def queue_size():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select * from performance_schema.global_variables where VARIABLE_NAME = "delayed_queue_size"')
        rows = cursor.fetchall()
        #print(rows)
        if(len(rows)>0):
            t=rows[0][1]
            ins =int(t)
        else:
            ins=0
        return ins

    except Error as e:
        return -1
    conn.close()



def pages_read():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select variable_value from sys.metrics where VARIABLE_NAME = "Innodb_pages_read"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()




def pages_written():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select variable_value from sys.metrics where VARIABLE_NAME = "Innodb_pages_written"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()



def read_latency():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select io_read_latency from sys.schema_table_statistics')
        rows = cursor.fetchall()
        ins = 0
        for row in rows:
            t = rows[0][0].split(' ')[0].split('.')
            ins += (int(t[0])*(10**len(t[1]))+int(t[1]))*(10**(-1*len(t[1])))
        return ins

    except Error as e:
        return -1
    conn.close()



def write_latency():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select io_write_latency from sys.schema_table_statistics')
        rows = cursor.fetchall()
        ins=0
        for row in rows:
            ins += int(rows[0][0].split(' ')[0])
        return ins

    except Error as e:
        return -1
    conn.close()



def cache_miss_rate():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select VARIABLE_VALUE from sys.metrics where VARIABLE_NAME = "innodb_buffer_pool_reads"')
        rows = cursor.fetchall()
        in1 =int(rows[0][0])
        cursor = conn.cursor()
        cursor.execute('select VARIABLE_VALUE from metrics where VARIABLE_NAME = "innodb_buffer_pool_read_requests"')
        rows = cursor.fetchall()
        in2 =int(rows[0][0])
        rate = in1*100/in2
        return rate

    except Error as e:
        return -1
    conn.close()




def current_memory():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select current_memory from sys.processlist')
        rows = cursor.fetchall()
        ins = 0
        for row in rows:
            ins += int(rows[0][0].split(' ')[0])
        return ins

    except Error as e:
        return -1
    conn.close()


def cpu_system():
    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    try:
        cursor = conn.cursor()
        cursor.execute('select cpu_system from information_schema.profiling')
        rows = cursor.fetchall()
        ins = 0
        for row in rows:
            ins += int(rows[0][0])
        return ins

    except Error as e:
        return -1
    conn.close()

#################################################################################

fig5 = plt.figure()

ax_queue_size = fig5.add_subplot(141)
xdata_queue_size, ydata_queue_size = [], []
ln_queue_size, = ax_queue_size.plot([], [], 'r-')

ax_pages_read = fig5.add_subplot(142)
xdata_pages_read, ydata_pages_read = [], []
ln_pages_read, = ax_pages_read.plot([], [], 'g-')

ax_pages_written = fig5.add_subplot(143)
xdata_pages_written, ydata_pages_written = [], []
ln_pages_written, = ax_pages_written.plot([], [], 'r-')

ax_read_latency = fig5.add_subplot(144)
xdata_read_latency, ydata_read_latency = [], []
ln_read_latency, = ax_read_latency.plot([], [], 'r-')

def init5():
    ln_queue_size.set_data([], [])
    ln_pages_read.set_data([], [])
    ln_pages_written.set_data([], [])
    ln_read_latency.set_data([], [])
    
    ax_queue_size.set_xlim(0, 50)
    ax_queue_size.set_ylim(-2, 100)
    ax_queue_size.set_ylabel('queue size')

    ax_pages_read.set_xlim(0, 50)
    ax_pages_read.set_ylim(-2, 100)
    ax_pages_read.set_ylabel('pages read')
    
    ax_pages_written.set_xlim(0, 50)
    ax_pages_written.set_ylim(-2, 100)
    ax_pages_written.set_ylabel('pages written')

    ax_read_latency.set_xlim(0, 50)
    ax_read_latency.set_ylim(-2, 100)
    ax_read_latency.set_ylabel('read latency')

    return ln_queue_size, ln_pages_read, ln_pages_written, ln_read_latency,

def update5(frame):
    xdata_queue_size.append(time.clock())
    ydata_queue_size.append(queue_size())
    ln_queue_size.set_data(xdata_queue_size, ydata_queue_size)

    xdata_pages_read.append(time.clock())
    ydata_pages_read.append(pages_read())
    ln_pages_read.set_data(xdata_pages_read, ydata_pages_read)

    xdata_pages_written.append(time.clock())
    ydata_pages_written.append(pages_written())
    ln_pages_written.set_data(xdata_pages_written, ydata_pages_written)

    xdata_read_latency.append(time.clock())
    ydata_read_latency.append(read_latency())
    ln_read_latency.set_data(xdata_read_latency, ydata_read_latency)

    return  ln_queue_size, ln_pages_read, ln_pages_written, ln_read_latency,

ani5 = FuncAnimation(fig5, update5, frames=1,
                    init_func=init5, blit=True, interval =80)
plt.show()
##################################################################################
fig6 = plt.figure()

ax_write_latency = fig6.add_subplot(141)
xdata_write_latency, ydata_write_latency = [], []
ln_write_latency, = ax_write_latency.plot([],[], 'r-')

ax_cache_miss_rate = fig6.add_subplot(142)
xdata_cache_miss_rate, ydata_cache_miss_rate = [], []
ln_cache_miss_rate, = ax_cache_miss_rate.plot([], [], 'g-')

ax_current_memory = fig6.add_subplot(143)
xdata_current_memory, ydata_current_memory = [], []
ln_current_memory, = ax_current_memory.plot([], [], 'r-')

ax_cpu_system = fig6.add_subplot(144)
xdata_cpu_system, ydata_cpu_system = [], []
ln_cpu_system, = ax_cpu_system.plot([], [], 'r-')

def init6():
    ln_write_latency.set_data([], [])
    ln_cache_miss_rate.set_data([], [])
    ln_current_memory.set_data([], [])
    ln_cpu_system.set_data([], [])
    
    ax_write_latency.set_xlim(0, 50)
    ax_write_latency.set_ylim(-2, 100)
    ax_write_latency.set_ylabel('write latency')

    ax_cache_miss_rate.set_xlim(0, 50)
    ax_cache_miss_rate.set_ylim(-2, 100)
    ax_cache_miss_rate.set_ylabel('cache miss rate')
    
    ax_current_memory.set_xlim(0, 50)
    ax_current_memory.set_ylim(-2, 100)
    ax_current_memory.set_ylabel('current memory')

    ax_cpu_system.set_xlim(0, 50)
    ax_cpu_system.set_ylim(-2, 100)
    ax_cpu_system.set_ylabel('cpu system')

    return ln_write_latency, ln_cache_miss_rate, ln_current_memory, ln_cpu_system,

def update6(frame):
    xdata_write_latency.append(time.clock())
    ydata_write_latency.append(write_latency())
    ln_write_latency.set_data(xdata_write_latency, ydata_write_latency)

    xdata_cache_miss_rate.append(time.clock())
    ydata_cache_miss_rate.append(cache_miss_rate())
    ln_cache_miss_rate.set_data(xdata_cache_miss_rate, ydata_cache_miss_rate)

    xdata_current_memory.append(time.clock())
    ydata_current_memory.append(current_memory())
    ln_current_memory.set_data(xdata_current_memory, ydata_current_memory)

    xdata_cpu_system.append(time.clock())
    ydata_cpu_system.append(cpu_system())
    ln_cpu_system.set_data(xdata_cpu_system, ydata_cpu_system)

    return  ln_write_latency, ln_cache_miss_rate, ln_current_memory, ln_cpu_system,

ani6 = FuncAnimation(fig6, update6, frames=1,
                    init_func=init6, blit=True, interval =80)


################################################################################
plt.show()


