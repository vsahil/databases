from mysql.connector import MySQLConnection, Error
from python_mysql_dbconfig import read_db_config
 


def memory_global_total():
	try:
	    cursor = conn.cursor()
	    cursor.execute("select * from sys.memory_global_total")
	    rows = cursor.fetchall()
	    for row in rows:
	        r = row[0]
	        mem_gl = int(float(str(r.split()[0])))
	        print(mem_gl)

	except Error as e:
	    print(e)


def buffer_pages():
	try:
		cursor = conn.cursor()
		cursor.execute("select pages from sys.innodb_buffer_stats_by_schema")
		rows = cursor.fetchall()
		buf_pag = 0
		for row in rows:
			buf_pag += int(row[0])
		
		print(buf_pag)

	except Error as e:
	    print(e)



def lock_wait():
	try:
		cursor = conn.cursor()
		cursor.execute("select * from sys.innodb_lock_waits")
		rows = cursor.fetchall()
		print len(rows)

	except Error as e:
	    print(e)



def temp_table():
	try:
		cursor = conn.cursor()
		cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "created_tmp_tables"')
		rows = cursor.fetchall()
		ins =int(rows[0][0])
		print(ins)

	except Error as e:
	    print(e)



def dirty_pages():
	try:
		cursor = conn.cursor()
		cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "innodb_buffer_pool_pages_dirty"')
		rows = cursor.fetchall()
		ins =int(rows[0][0])
		print(ins)

	except Error as e:
	    print(e)


def pages_data():
	try:
		cursor = conn.cursor()
		cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "innodb_buffer_pool_pages_data"')
		rows = cursor.fetchall()
		ins =int(rows[0][0])
		print(ins)

	except Error as e:
	    print(e)



def write_req_buff():
	try:
		cursor = conn.cursor()
		cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "innodb_buffer_pool_write_requests"')
		rows = cursor.fetchall()
		ins =int(rows[0][0])
		print(ins)

	except Error as e:
	    print(e)



def pages_written():
	try:
		cursor = conn.cursor()
		cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "innodb_pages_written"')
		rows = cursor.fetchall()
		ins =int(rows[0][0])
		print(ins)

	except Error as e:
	    print(e)


def queries():
	try:
		cursor = conn.cursor()
		cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "queries"')
		rows = cursor.fetchall()
		ins =int(rows[0][0])
		print(ins)

	except Error as e:
	    print(e)



def full_join():
	try:
		cursor = conn.cursor()
		cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "select_full_join"')
		rows = cursor.fetchall()
		ins =int(rows[0][0])
		print(ins)

	except Error as e:
	    print(e)


def sort_merge_passes():
	try:
		cursor = conn.cursor()
		cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "sort_merge_passes"')
		rows = cursor.fetchall()
		ins =int(rows[0][0])
		print(ins)

	except Error as e:
	    print(e)



def table_wait_lock():
	try:
		cursor = conn.cursor()
		cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "table_locks_waited"')
		rows = cursor.fetchall()
		ins =int(rows[0][0])
		print(ins)

	except Error as e:
	    print(e)



def threads_running():
	try:
		cursor = conn.cursor()
		cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "threads_running"')
		rows = cursor.fetchall()
		ins =int(rows[0][0])
		print(ins)

	except Error as e:
	    print(e)



def locks_deadlocks():
	try:
		cursor = conn.cursor()
		cursor.execute('select m.Variable_value from sys.metrics m where m.Variable_name = "locks_deadlocks"')
		rows = cursor.fetchall()
		ins =int(rows[0][0])
		print(ins)

	except Error as e:
	    print(e)



def full_table_scan():
	try:
		cursor = conn.cursor()
		cursor.execute("select * from sys.statements_with_full_table_scans")
		rows = cursor.fetchall()
		print len(rows)

	except Error as e:
	    print(e)


def sorting():
	try:
		cursor = conn.cursor()
		cursor.execute("select * from sys.statements_with_sorting")
		rows = cursor.fetchall()
		print len(rows)

	except Error as e:
	    print(e)



def row_update():
	try:
		cursor = conn.cursor()
		cursor.execute('select Variable_value from sys.metrics m where m.Variable_name = "innodb_rows_updated"')
		rows = cursor.fetchall()
		buf_pag = 0
		for row in rows:
			ins = row[0]
		ins =int(ins)
		print(ins)

	except Error as e:
	    print(e)



def query_with_fetchall():
	try:
	    cursor = conn.cursor()
	    cursor.execute("SHOW GLOBAL STATUS;")
	    # cursor.execute("select * from information_schema.statistics")
	    # cursor.execute("select * from sys.memory_global_total")
	    rows = cursor.fetchall()
	    print('Total Row(s):', cursor.rowcount)
	    for row in rows:
	        print(row)

	except Error as e:
	    print(e)

 
 
if __name__ == '__main__':
	dbconfig = read_db_config()
	conn = MySQLConnection(**dbconfig)
	# memory_global_total()
	# buffer_pages()
	# row_update()
	# lock_wait()
	# full_table_scan()
	# sorting()
	# temp_table()
	# dirty_pages()
	# pages_data()
	# write_req_buff()
	# pages_written()
	# queries()
	# full_join()
	# sort_merge_passes()
	# table_wait_lock()
	# threads_running()
	# locks_deadlocks()
	# query_with_fetchall()
	# cursor.close()
	conn.close()




# def query_with_fetchone():
#     try:
#         dbconfig = read_db_config()
#         conn = MySQLConnection(**dbconfig)
#         cursor = conn.cursor()
#         cursor.execute("SELECT * FROM books")
 
#         row = cursor.fetchone()
 
#         while row is not None:
#             print(row)
#             row = cursor.fetchone()
 
#     except Error as e:
#         print(e)
 
#     finally:
#         cursor.close()
#         conn.close()
 
 
# if __name__ == '__main__':
#     query_with_fetchone()
