from mysql.connector import MySQLConnection, Error
from python_mysql_dbconfig import read_db_config
# import pyqtgraph as pg
import numpy as np



def queue_size():
    try:
        cursor = conn.cursor()
        cursor.execute('select * from performance_schema.global_variables where VARIABLE_NAME = "delayed_queue_size"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        print(ins)

    except Error as e:
        print(e)



def pages_read():
    try:
        cursor = conn.cursor()
        cursor.execute('select variable_value from sys.metrics where VARIABLE_NAME = "Innodb_pages_read"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        print(ins)

    except Error as e:
        print(e)




def pages_written():
    try:
        cursor = conn.cursor()
        cursor.execute('select variable_value from sys.metrics where VARIABLE_NAME = "Innodb_pages_written"')
        rows = cursor.fetchall()
        ins =int(rows[0][0])
        print(ins)

    except Error as e:
        print(e)



def read_latency():
    try:
        cursor = conn.cursor()
        cursor.execute('select io_read_latency from sys.schema_table_statistics')
        rows = cursor.fetchall()
        for row in rows:
            ins += int(rows[0][0])
        print(ins)

    except Error as e:
        print(e)



def write_latency():
    try:
        cursor = conn.cursor()
        cursor.execute('select io_write_latency from sys.schema_table_statistics')
        rows = cursor.fetchall()
        for row in rows:
            ins += int(rows[0][0])
        print(ins)

    except Error as e:
        print(e)



def cache_miss_rate():
    try:
        cursor = conn.cursor()
        cursor.execute('select VARIABLE_VALUE from sys.metrics where VARIABLE_NAME = "innodb_buffer_pool_reads"')
        rows = cursor.fetchall()
        in1 =int(rows[0][0])
        cursor = conn.cursor()
        cursor.execute('select VARIABLE_VALUE from metrics where VARIABLE_NAME = "innodb_buffer_pool_read_requests"')
        rows = cursor.fetchall()
        in2 =int(rows[0][0])
        rate = in1*100/in2
        print(rate)

    except Error as e:
        print(e)




def current_memory():
    try:
        cursor = conn.cursor()
        cursor.execute('select current_memory from sys.processlist')
        rows = cursor.fetchall()
        ins = 0
        for row in rows:
            ins += int(rows[0][0])
        print(ins)

    except Error as e:
        print(e)


# def call_avg_latency():
#     try:
#         cursor = conn.cursor()
#         cursor.execute('CALL ps_statement_avg_latency_histogram()')
#         rows = cursor.fetchall()
#         ins = 0
#         for row in rows:
#             ins += int(rows[0][0])
#         print(ins)

#     except Error as e:
#         print(e)



def cpu_system():
    try:
        cursor = conn.cursor()
        cursor.execute('select cpu_system from information_schema.profiling')
        rows = cursor.fetchall()
        ins = 0
        for row in rows:
            ins += int(rows[0][0])
        print(ins)

    except Error as e:
        print(e)


def call_find_by_isbn():
    try:
        i=0
        db_config = read_db_config()
        conn = MySQLConnection(**db_config)
        cursor = conn.cursor()
 
        '''args = ['1236400967773', 0]'''
        # cursor.callproc('python_mysql.os_run2')
        cursor.execute("select * from sys.schema_table_statistics")
        rows = cursor.fetchall()
        for row in rows:
            print row
 
        # for result in cursor.stored_results():
        #      a=result.fetchall()

        # d=[]
        # d2=[1,2,3,4]     
        # for i in range(len(a)):
        #     d.append(float(a[i][13][:-3]))
        #d=d.split(" ")
        print(d)
            
        # pg.plot(d2,d, pen='r')
        # pg.plot(d2,d2, pen='b') 
        # pg.QtGui.QApplication.exec_()

        #pg.plot(d2,d2, pen='b') 
        #pg.QtGui.QApplication.exec_()
        #pg.show(d)
        s=input("Plotted")    
        '''print(result)'''
 
    except Error as e:
        print(e)
 
    finally:
        cursor.close()
        conn.close()
 
if __name__ == '__main__':
    queue_size()
    call_find_by_isbn()

